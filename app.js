"use strict";

var express = require("express");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var favicon = require("serve-favicon");
var logger = require("morgan");
var path = require("path");

var auth = require(path.join(__dirname, "helpers", "auth"));
var handlers = require(path.join(__dirname, "helpers", "handlers"));

var blank = require(path.join(__dirname, "controllers", "blank"));
var index = require(path.join(__dirname, "controllers", "index"));
var talks = require(path.join(__dirname, "controllers", "talks"));
var workshops = require(path.join(__dirname, "controllers", "workshops"));
var others = require(path.join(__dirname, "controllers", "others"));
var devrooms = require(path.join(__dirname, "controllers", "devrooms"));
//var form = require(path.join(__dirname, "controllers", "form"));

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// uncomment after placing your favicon in /public
app.use(favicon(path.join(__dirname, "assets", "favicon.ico")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "assets")));

app.use("/", blank);
//app.use("/form", form);

app.use(auth());
app.use("/admin/", index);
app.use("/admin/talks", talks);
app.use("/admin/workshops", workshops);
app.use("/admin/others", others);
app.use("/admin/devrooms", devrooms);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  handlers.errorHandlerCreate(res, err);
});

module.exports = app;
