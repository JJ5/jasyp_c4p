"use strict";

require("dotenv").config();

var express = require("express");
var path = require("path");

var router = express.Router();

var config = require(path.join(__dirname, "..", "config", "config"));
var models = require(path.join(__dirname, "..", "models"));

var handlers = require(path.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(path.join(__dirname, "..", "helpers", "slugify"));
var gitlab = require(path.join(__dirname, "..", "helpers", "gitlab"));

router.get("/create", function(req, res, next) {
  handlers.renderView(res, "talks/create", " - Registro de charlas");
});

router.get("/test", function(req, res, next) {
  handlers.renderView(res, "talks/test", " - Registro de charlas (boiler)");
});

router.post("/create", async (req, res) => {
  let values = {
    title: req.body.title.replace(/:/g, ''),
    abstract: req.body.abstract,
    format: req.body.format,
    description: req.body.description,
    project: req.body.project,
    target: req.body.target,
    speakers: req.body.speakers,
    name: req.body.name,
    email: req.body.email,
    web: req.body.web,
    mastodon: req.body.mastodon,
    twitter: req.body.twitter,
    gitlab: req.body.gitlab,
    github: req.body.github,
    comments: req.body.comments,
    privacy_email: req.body.privacy_email == "on" ? true : false,
    privacy_social: req.body.privacy_social == "on" ? true : false,
    accept_coc: req.body.accept_coc == "on" ? true : false,
    accept_attend: req.body.accept_attend == "on" ? true : false,
    state: "R"
  };

  models.Talk.create(values).then(function() {
    let file = slugify.getFileName(values.title) + ".md";
    let branch = slugify.getBranchName(values.name, values.title);

    gitlab.apiActions(config.type.talk, branch, file, values, res);
  });
});

module.exports = router;