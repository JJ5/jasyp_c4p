"use strict";

require("dotenv").config();

var express = require("express");
var path = require("path");

var router = express.Router();

var config = require(path.join(__dirname, "..", "config", "config"));
var models = require(path.join(__dirname, "..", "models"));

var handlers = require(path.join(__dirname, "..", "helpers", "handlers"));
var slugify = require(path.join(__dirname, "..", "helpers", "slugify"));
var gitlab = require(path.join(__dirname, "..", "helpers", "gitlab"));

router.get("/create", function(req, res, next) {
  handlers.renderView(res, "devrooms/create", " - Registro de salas");
});

router.post("/create", async (req, res) => {
  let values = {
    name: req.body.name.replace(/:/g, ''),
    description: req.body.description,
    community: req.body.community,
    web: req.body.web,
    mastodon: req.body.mastodon,
    twitter: req.body.twitter,
    gitlab: req.body.gitlab,
    github: req.body.github,
    contact: req.body.contact,
    email: req.body.email,
    target: req.body.target,
    length: req.body.length,
    day: req.body.day,
    format: req.body.format,
    comments: req.body.comments,
    privacy_email: req.body.privacy_email == "on" ? true : false,
    accept_coc: req.body.accept_coc == "on" ? true : false,
    accept_attend: req.body.accept_attend == "on" ? true : false,
    accept_coordination: req.body.accept_coordination == "on" ? true : false,
    accept_timelimit: req.body.accept_timelimit == "on" ? true : false,
    state: "R"
  };

  models.Devroom.create(values).then(function() {
    let file = slugify.getFileName(values.name) + ".md";
    let branch = slugify.getBranchName(values.contact, values.name);

    gitlab.apiActions(config.type.devroom, branch, file, values, res);
  });
});

module.exports = router;
