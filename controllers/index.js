"use strict";

var express = require("express");
var path = require("path");

var router = express.Router();

var config = require(path.join(__dirname, "..", "config", "config"));

router.get("/", function(req, res, next) {
  res.render("pages/index", {
    title: config.event_name
  });
});

module.exports = router;
