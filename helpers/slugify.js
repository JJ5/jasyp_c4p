"use strict";

const REGEX = '/[!"#%&()*+,-./:;<=>?\[\\\]^`{|}~¡¬·¿─$€½]/g';

var slugify = require('slugify');

slugify.extend({
  '%': '%',
  '&': '&',
  '<': '<',
  '>': '>',
  '@': 'a',
  '|': '|',
  '$': '$',
  '€': '€',
  'ç': 'c'
});

function getFileName(title) {
  return slugify(title, {
    remove: REGEX,
    lower: true
  }).slice(0, 64);
}

function getBranchName(name, title) {
  return slugify(name, {
    remove: REGEX,
    lower: true
  }).slice(0, 12) + "-" + getFileName(title).slice(0, 12);
}

module.exports = {
  getFileName,
  getBranchName
};