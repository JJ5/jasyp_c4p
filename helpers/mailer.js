"use strict";

var path = require("path");
var nodemailer = require("nodemailer");

var config = require(path.join(__dirname, "..", "config", "config"));

var slugify = require(path.join(__dirname, "..", "helpers", "slugify"));
var templates = require(path.join(__dirname, "..", "helpers", "templates"));

function configMailer(name, email, subject, html, filename, content) {
  return nodemailer.createTransport({
    host: config.email.smtp + "." + config.email.server,
    port: 25,
    secure: false,
    requireTLS: true,
    auth: {
      user: config.email.user,
      pass: config.email.password
    }
  }, {
    from: config.email.name + " <" + config.email.user + ">",
    bcc: config.email.bcc,
    to: name.concat(" <", email, ">"),
    organization: config.organization,
    subject: subject,
    html: html,
    attachments: [{
      filename: filename,
      content: content
    }]
  });
}

module.exports = {
  getMailer: function(type, values, merge_url) {
    let html, filename, content, subject, addressee;

    switch (type) {
      case config.type.talk:
        addressee = values.name;
        subject = "[esLibre 2021] Propuesta de charla";
        html = templates.getEsTalkMail(values, merge_url);
        filename = slugify.getFileName(values.title) + ".md";
        content = templates.getEsTalkTemplate(values);
        break;

      case config.type.workshop:
        addressee = values.name;
        subject = "[esLibre 2021] Propuesta de taller";
        html = templates.getEsWorkshopMail(values, merge_url);
        filename = slugify.getFileName(values.title) + ".md";
        content = templates.getEsWorkshopTemplate(values);
        break;

      case config.type.other:
        addressee = values.name;
        subject = "[esLibre 2021] Propuesta de actividad";
        html = templates.getEsOtherMail(values, merge_url);
        filename = slugify.getFileName(values.title) + ".md";
        content = templates.getEsOtherTemplate(values);
        break;

      case config.type.devroom:
        addressee = values.contact;
        subject = "[esLibre 2021] Propuesta de sala";
        html = templates.getEsDevroomMail(values, merge_url);
        filename = slugify.getFileName(values.name) + ".md";
        content = templates.getEsDevroomTemplate(values);
        break;

      default:
        handlers.errorHandlerCreate(res, new Error('[ERROR]: Type of proposal unknown.'));
    }

    return configMailer(addressee, values.email, subject, html, filename, content);
  }
};