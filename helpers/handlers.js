"use strict";

var path = require("path");

var config = require(path.join(__dirname, "..", "config", "config"));

function renderView(res, view, title) {
  res.render(view, {
    app_name: config.app_name,
    title: config.event_name + title
  });
}

function errorHandlerCreate(res, error) {
  res.render("error", {
    app_name: config.app_name,
    title: config.event_name + " - Error",
    message: error.message,
    error: error
  });
}

module.exports = {
  renderView,
  errorHandlerCreate
};
