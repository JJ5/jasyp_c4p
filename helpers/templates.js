"use strict";

var fs = require("fs");
var path = require("path");

function getEsTalkMail(values, merge_url) {
  let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", "eslibre", "talk.html"), "utf8");

  const replaces = {
    _merge_url: merge_url,
    _title: values.title,
    _abstract: values.abstract,
    _format: values.format,
    _description: values.description,
    _project: values.project,
    _target: values.target,
    _speakers: values.speakers,
    _name: values.name,
    _email: values.email,
    _web: values.web,
    _mastodon: values.mastodon,
    _twitter: values.twitter,
    _gitlab: values.gitlab,
    _github: values.github,
    _comments: values.comments,
    _privacy_email: values.privacy_email,
    _privacy_social: values.privacy_social
  };

  if (values.format === "S") {
    html = html.replace(/_format/gi, "Charla corta (10 minutos)");
  } else {
    html = html.replace(/_format/gi, "Charla (25 minutos)");
  }

  if (values.privacy_email == true) {
    html = html.replace(/_privacy_email/gi, "No");
  } else {
    html = html.replace(/_privacy_email/gi, "Sí");
  }

  if (values.privacy_social == true) {
    html = html.replace(/_privacy_social/gi, "No");
  } else {
    html = html.replace(/_privacy_social/gi, "Sí");
  }

  html = html.replace(/_merge_url|_title|_abstract|_description|_project|_target|_speakers|_name|_email|_web|_mastodon|_twitter|_gitlab|_github|_comments/gi, function(matched) {
    return replaces[matched];
  });

  html = html.replace(/twitter\.com\/@/gi, "twitter.com/");
  html = html.replace(/gitlab\.com\/@/gi, "gitlab.com/");

  return html;
}

function getEsWorkshopMail(values, merge_url) {
  let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", "eslibre", "workshop.html"), "utf8");

  const replaces = {
    _merge_url: merge_url,
    _title: values.title,
    _description: values.description,
    _goals: values.goals,
    _project: values.project,
    _target: values.target,
    _speakers: values.speakers,
    _name: values.name,
    _email: values.email,
    _web: values.web,
    _mastodon: values.mastodon,
    _twitter: values.twitter,
    _gitlab: values.gitlab,
    _github: values.github,
    _prerequisites_attendees: values.prerequisites_attendees,
    _prerequisites_organization: values.prerequisites_organization,
    _length: values.length,
    _day: values.day,
    _comments: values.comments,
    _privacy_email: values.privacy_email,
    _privacy_social: values.privacy_social
  };

  if (values.privacy_email == true) {
    html = html.replace(/_privacy_email/gi, "No");
  } else {
    html = html.replace(/_privacy_email/gi, "Sí");
  }

  if (values.privacy_social == true) {
    html = html.replace(/_privacy_social/gi, "No");
  } else {
    html = html.replace(/_privacy_social/gi, "Sí");
  }

  html = html.replace(/_merge_url|_title|_description|_goals|_project|_target|_speakers|_name|_email|_web|_mastodon|_twitter|_gitlab|_github|_prerequisites_attendees|_prerequisites_organization|_length|_day|_comments/gi, function(matched) {
    return replaces[matched];
  });

  html = html.replace(/twitter\.com\/@/gi, "twitter.com/");
  html = html.replace(/gitlab\.com\/@/gi, "gitlab.com/");

  return html;
}

function getEsOtherMail(values, merge_url) {
  let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", "eslibre", "other.html"), "utf8");

  const replaces = {
    _merge_url: merge_url,
    _title: values.title,
    _abstract: values.abstract,
    _type: values.type,
    _description: values.description,
    _project: values.project,
    _target: values.target,
    _speakers: values.speakers,
    _name: values.name,
    _email: values.email,
    _web: values.web,
    _mastodon: values.mastodon,
    _twitter: values.twitter,
    _gitlab: values.gitlab,
    _github: values.github,
    _prerequisites_attendees: values.prerequisites_attendees,
    _prerequisites_organization: values.prerequisites_organization,
    _comments: values.comments,
    _privacy_email: values.privacy_email,
    _privacy_social: values.privacy_social
  };

  if (values.privacy_email == true) {
    html = html.replace(/_privacy_email/gi, "No");
  } else {
    html = html.replace(/_privacy_email/gi, "Sí");
  }

  if (values.privacy_social == true) {
    html = html.replace(/_privacy_social/gi, "No");
  } else {
    html = html.replace(/_privacy_social/gi, "Sí");
  }

  html = html.replace(/_merge_url|_title|_abstract|_type|_description|_project|_target|_speakers|_name|_email|_web|_mastodon|_twitter|_gitlab|_github|_prerequisites_attendees|_prerequisites_organization|_comments/gi, function(matched) {
    return replaces[matched];
  });

  html = html.replace(/twitter\.com\/@/gi, "twitter.com/");
  html = html.replace(/gitlab\.com\/@/gi, "gitlab.com/");

  return html;
}

function getEsDevroomMail(values, merge_url) {
  let html = fs.readFileSync(path.join(__dirname, "..", "config", "mails", "eslibre", "devroom.html"), "utf8");

  const replaces = {
    _merge_url: merge_url,
    _name: values.name,
    _description: values.description,
    _community: values.community,
    _web: values.web,
    _mastodon: values.mastodon,
    _twitter: values.twitter,
    _gitlab: values.gitlab,
    _github: values.github,
    _contact: values.contact,
    _email: values.email,
    _target: values.target,
    _length: values.length,
    _day: values.day,
    _format: values.format,
    _comments: values.comments,
    _privacy_email: values.privacy_email
  };

  if (values.privacy_email == true) {
    html = html.replace(/_privacy_email/gi, "No");
  } else {
    html = html.replace(/_privacy_email/gi, "Sí");
  }

  html = html.replace(/_merge_url|_name|_description|_community|_web|_mastodon|_twitter|_gitlab|_github|_contact|_email|_target|_length|_day|_format|_comments/gi, function(matched) {
    return replaces[matched];
  });

  html = html.replace(/twitter\.com\/@/gi, "twitter.com/");
  html = html.replace(/gitlab\.com\/@/gi, "gitlab.com/");

  return html;
}

function getEsTalkTemplate(values) {
  let format_a = "-   [x]  Charla corta (10 minutos)\n-   [ ]  Charla (25 minutos)\n\n";
  let format_b = "-   [ ]  Charla corta (10 minutos)\n-   [x]  Charla (25 minutos)\n\n";
  let project_a = "-   Web del proyecto:\n\n";
  let project_b = "-   Web del proyecto: <" + values.project + ">\n\n";
  let email_a = "-   Email:\n";
  let email_b = "-   Email: " + values.email + "\n";
  let web_a = "-   Web personal:\n";
  let web_b = "-   Web personal: <" + values.web + ">\n";
  let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
  let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
  let twitter_a = "-   Twitter:\n";
  let twitter_b = "-   Twitter: <" + values.twitter.replace(/@/gi, "https://twitter.com/") + ">\n";
  let gitlab_a = "-   Gitlab:\n";
  let gitlab_b = "-   Gitlab: <" + values.gitlab.replace(/@/gi, "https://gitlab.com/") + ">\n";
  let github_a = "-   Portfolio o GitHub (u otros sitios de código colaborativo):\n\n";
  let github_b = "-   Portfolio o GitHub (u otros sitios de código colaborativo): <" + values.github + ">\n\n";
  let privacy_email_a = "-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.\n";
  let privacy_email_b = "-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.\n";
  let privacy_social_a = "-   [ ]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.\n\n";
  let privacy_social_b = "-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.\n\n";
  let accept_coc_txt = "-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.\n";
  let accept_attend_txt = "-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.";

  let template = "---\n" +
    "layout: 2021/post\n" +
    "section: proposals\n" +
    "category: talks\n" +
    "title: " + values.title + "\n" +
    "---\n\n";

  template += values.abstract + "\n\n";

  template += "## Formato de la propuesta\n\n" +
    "Indicar uno de estos:\n";
  template += values.format === "S" ? format_a : format_b;

  template += "## Descripción\n\n" + values.description + "\n\n";

  template += values.project === "" ? project_a : project_b;

  template += "## Público objetivo\n\n" + values.target + "\n\n";

  template += "## Ponente(s)\n\n" + values.speakers + "\n\n";

  template += "### Contacto(s)\n\n" +
    "-   Nombre: " + values.name + "\n";
  template += values.privacy_email === true ? email_a : email_b;
  template += values.web === "" ? web_a : web_b;
  if (values.privacy_social) {
    template += mastodon_a + twitter_a;
  } else {
    template += values.mastodon === "" ? mastodon_a : mastodon_b;
    template += values.twitter === "" ? twitter_a : twitter_b;
  }
  template += values.gitlab === "" ? gitlab_a : gitlab_b;
  template += values.github === "" ? github_a : github_b;

  template += "## Comentarios\n\n" + values.comments + "\n\n";

  template += "## Privacidad\n\n";
  template += values.privacy_email == true ? privacy_email_a : privacy_email_b;
  template += values.privacy_social == true ? privacy_social_a : privacy_social_b;

  template += "## Condiciones\n\n" + accept_coc_txt + accept_attend_txt + "\n";

  return template;
}

function getEsWorkshopTemplate(values) {
  let project_a = "-   Web del proyecto:\n\n";
  let project_b = "-   Web del proyecto: <" + values.project + ">\n\n";
  let email_a = "-   Email:\n";
  let email_b = "-   Email: " + values.email + "\n";
  let web_a = "-   Web personal:\n";
  let web_b = "-   Web personal: <" + values.web + ">\n";
  let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
  let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
  let twitter_a = "-   Twitter:\n";
  let twitter_b = "-   Twitter: <" + values.twitter.replace(/@/gi, "https://twitter.com/") + ">\n";
  let gitlab_a = "-   Gitlab:\n";
  let gitlab_b = "-   Gitlab: <" + values.gitlab.replace(/@/gi, "https://gitlab.com/") + ">\n";
  let github_a = "-   Portfolio o GitHub (u otros sitios de código colaborativo):\n\n";
  let github_b = "-   Portfolio o GitHub (u otros sitios de código colaborativo): " + values.github + "\n\n";
  let privacy_email_a = "-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información del taller.\n";
  let privacy_email_b = "-   [x]  Doy permiso para que mi email de contacto sea publicado con la información del taller.\n";
  let privacy_social_a = "-   [ ]  Doy permiso para que mis redes sociales sean publicadas con la información del taller.\n\n";
  let privacy_social_b = "-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información del taller.\n\n";
  let accept_coc_txt = "-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.\n";
  let accept_coordination_txt = "-   [x]  Acepto coordinarme con la organización de esLibre para la realización del taller.\n";
  let accept_attend_txt = "-   [x]  Confirmo que al menos una persona de entre las que proponen el taller estará conectada el día programado para impartirlo.\n";

  let template = "---\n" +
    "layout: 2021/post\n" +
    "section: proposals\n" +
    "category: workshops\n" +
    "title: " + values.title + "\n" +
    "---\n\n";

  template += "## Descripción\n\n" + values.description + "\n\n" +
    "## Objetivos a cubrir en el taller\n\n" + values.goals + "\n\n";

  template += values.project === "" ? project_a : project_b;

  template += "## Público objetivo\n\n" + values.target + "\n\n" +
    "## Ponente(s)\n\n" + values.speakers + "\n\n";

  template += "### Contacto(s)\n\n" + "-   Nombre: " + values.name + "\n";

  template += values.privacy_email === true ? email_a : email_b;

  template += values.web === "" ? web_a : web_b;

  if (values.privacy_social) {
    template += mastodon_a + twitter_a;
  } else {
    template += values.mastodon === "" ? mastodon_a : mastodon_b;
    template += values.twitter === "" ? twitter_a : twitter_b;
  }

  template += values.gitlab === "" ? gitlab_a : gitlab_b;
  template += values.github === "" ? github_a : github_b;

  template += "## Prerrequisitos para los asistentes\n\n" + values.prerequisites_attendees + "\n\n" +
    "## Prerrequisitos para la organización\n\n" + values.prerequisites_organization + "\n\n" +
    "-   Duración: " + values.length + "\n" +
    "-   Día: " + values.day + "\n\n" +
    "## Comentarios\n\n" +
    values.comments + "\n\n";

  template += "## Privacidad\n\n";
  template += values.privacy_email == true ? privacy_email_a : privacy_email_b;
  template += values.privacy_social == true ? privacy_social_a : privacy_social_b;

  template += "## Condiciones\n\n" + accept_coc_txt + accept_coordination_txt + accept_attend_txt;

  return template;
}

function getEsOtherTemplate(values) {
  let project_a = "-   Web del proyecto:\n\n";
  let project_b = "-   Web del proyecto: <" + values.project + ">\n\n";
  let email_a = "-   Email:\n";
  let email_b = "-   Email: " + values.email + "\n";
  let web_a = "-   Web personal:\n";
  let web_b = "-   Web personal: <" + values.web + ">\n";
  let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
  let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
  let twitter_a = "-   Twitter:\n";
  let twitter_b = "-   Twitter: <" + values.twitter.replace(/@/gi, "https://twitter.com/") + ">\n";
  let gitlab_a = "-   Gitlab:\n";
  let gitlab_b = "-   Gitlab: <" + values.gitlab.replace(/@/gi, "https://gitlab.com/") + ">\n";
  let github_a = "-   Portfolio o GitHub (u otros sitios de código colaborativo):\n\n";
  let github_b = "-   Portfolio o GitHub (u otros sitios de código colaborativo): " + values.github + "\n\n";
  let privacy_email_a = "-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la actividad.\n";
  let privacy_email_b = "-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la actividad.\n";
  let privacy_social_a = "-   [ ]  Doy permiso para que mis redes sociales sean publicadas con la información de la actividad.\n\n";
  let privacy_social_b = "-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la actividad.\n\n";
  let accept_coc_txt = "-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.\n";
  let accept_attend_txt = "-   [x]  Confirmo que al menos una persona de entre las que proponen la actividad estará conectada el día programado para realizarla.\n";

  let template = "---\n" +
    "layout: 2021/post\n" +
    "section: proposals\n" +
    "category: others\n" +
    "title: " + values.title + "\n" +
    "---\n\n";

  template += "## Resumen\n\n" + values.abstract + "\n\n" +
    "## Tipo de actividad\n\n" + values.type + "\n\n" +
    "## Descripción\n\n" + values.description + "\n\n";

  template += values.project === "" ? project_a : project_b;

  template += "## Público objetivo\n\n" + values.target + "\n\n" +
    "## Ponente(s)\n\n" + values.speakers + "\n\n";

  template += "### Contacto(s)\n\n" + "-   Nombre: " + values.name + "\n";

  template += values.privacy_email === true ? email_a : email_b;

  template += values.web === "" ? web_a : web_b;

  if (values.privacy_social) {
    template += mastodon_a + twitter_a;
  } else {
    template += values.mastodon === "" ? mastodon_a : mastodon_b;
    template += values.twitter === "" ? twitter_a : twitter_b;
  }

  template += values.gitlab === "" ? gitlab_a : gitlab_b;
  template += values.github === "" ? github_a : github_b;

  template += "## Prerrequisitos para los asistentes\n\n" + values.prerequisites_attendees + "\n\n" +
    "## Prerrequisitos para la organización\n\n" + values.prerequisites_organization + "\n\n" +
    "## Comentarios\n\n" + values.comments + "\n\n";

  template += "## Privacidad\n\n";
  template += values.privacy_email == true ? privacy_email_a : privacy_email_b;
  template += values.privacy_social == true ? privacy_social_a : privacy_social_b;

  template += "## Condiciones\n\n" + accept_coc_txt + accept_attend_txt;

  return template;
}

function getEsDevroomTemplate(values) {
  let web_a = "-   Web de la comunidad:\n";
  let web_b = "-   Web de la comunidad: <" + values.web + ">\n";
  let mastodon_a = "-   Mastodon (u otras redes sociales libres):\n";
  let mastodon_b = "-   Mastodon (u otras redes sociales libres): <" + values.mastodon + ">\n";
  let twitter_a = "-   Twitter:\n";
  let twitter_b = "-   Twitter: <" + values.twitter.replace(/@/gi, "https://twitter.com/") + ">\n";
  let gitlab_a = "-   Gitlab:\n";
  let gitlab_b = "-   Gitlab: <" + values.gitlab.replace(/@/gi, "https://gitlab.com/") + ">\n";
  let github_a = "-   Portfolio o GitHub (u otros sitios de código colaborativo):\n\n";
  let github_b = "-   Portfolio o GitHub (u otros sitios de código colaborativo): " + values.github + "\n\n";
  let contact_a = "-   Nombre de contacto:\n-   Email de contacto:\n\n";
  let contact_b = "-   Nombre de contacto: " + values.contact + "\n-   Email de contacto: " + values.email + "\n\n";
  let privacy_email_a = "-   [ ]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.\n\n"
  let privacy_email_b = "-   [x]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.\n\n"
  let accept_coc_txt = "-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.\n";
  let accept_coordination_txt = "-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.\n";
  let accept_timelimit_txt = "-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.\n";
  let accept_attend_txt = "-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.\n";

  let template = "---\n" +
    "layout: 2021/post\n" +
    "section: proposals\n" +
    "category: devrooms\n" +
    "title: " + values.name + "\n" +
    "---\n\n";

  template += "## Descripción\n\n" + values.description + "\n\n" +
    "## Comunidad o grupo que lo propone\n\n" + values.community + "\n\n";

  template += values.project === "" ? web_a : web_b;

  template += values.mastodon === "" ? mastodon_a : mastodon_b;
  template += values.twitter === "" ? twitter_a : twitter_b;

  template += values.gitlab === "" ? gitlab_a : gitlab_b;
  template += values.github === "" ? github_a : github_b;

  template += "### Contacto(s)\n\n";
  template += values.privacy_email === true ? contact_a : contact_b;

  template += "## Público objetivo\n\n" + values.target + "\n\n" +
    "## Formato\n\n" + values.format + "\n\n" +
    "-   Duración: " + values.length + "\n" +
    "-   Día: " + values.day + "\n\n" +
    "## Comentarios\n\n" + values.comments + "\n\n";

  template += "## Privacidad\n\n";
  template += values.privacy_email == true ? privacy_email_a : privacy_email_b;

  template += "## Condiciones\n\n" + accept_coc_txt + accept_coordination_txt + accept_timelimit_txt + accept_attend_txt;

  return template;
}

module.exports = {
  getEsTalkTemplate,
  getEsWorkshopTemplate,
  getEsOtherTemplate,
  getEsDevroomTemplate,
  getEsTalkMail,
  getEsWorkshopMail,
  getEsOtherMail,
  getEsDevroomMail
};
