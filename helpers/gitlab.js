"use strict";

const GITLAB_COMMIT_LENGTH = 72;

const util = require('util');

const {
  Gitlab
} = require('@gitbeaker/node');

const API = new Gitlab({
  token: process.env.GITLAB_TOKEN,
});

var path = require("path");

var config = require(path.join(__dirname, "..", "config", "config"));

var templates = require(path.join(__dirname, "..", "helpers", "templates"));
var handlers = require(path.join(__dirname, "..", "helpers", "handlers"));
var mailer = require(path.join(__dirname, "..", "helpers", "mailer"));

module.exports = {
  apiActions: async function(type, branch, file, values, res) {
    let commit_text_pre, merge_text_pre, content, id;

    switch (type) {
      case config.type.talk:
        commit_text_pre = "Crea charla: ";
        merge_text_pre = "Añade charla: ";
        content = templates.getEsTalkTemplate(values);
        break;
      case config.type.workshop:
        commit_text_pre = "Crea taller: ";
        merge_text_pre = "Añade taller: ";
        content = templates.getEsWorkshopTemplate(values);
        break;
      case config.type.other:
        commit_text_pre = "Crea actividad: ";
        merge_text_pre = "Añade actividad: ";
        content = templates.getEsOtherTemplate(values);
        break;
      case config.type.devroom:
        commit_text_pre = "Crea sala: ";
        merge_text_pre = "Añade sala: ";
        content = templates.getEsDevroomTemplate(values);
        break;
      default:
        handlers.errorHandlerCreate(res, new Error('[ERROR]: Type of proposal unknown.'));
    }

    if (type == config.type.devroom) {
      id = values.gitlab === "" ? values.name : "(cc: " + values.gitlab + ") " + values.name;
    } else {
      id = values.gitlab === "" ? values.name + " - " + values.title : values.gitlab + " - " + values.title;
    }

    let title_lenght = GITLAB_COMMIT_LENGTH - commit_text_pre.length;
    let commit_text = commit_text_pre + id.slice(0, title_lenght);
    let merge_text = merge_text_pre + id.slice(0, title_lenght);

    await API.Branches.create(process.env.GITLAB_PROJECT, branch, "master")
      .then((branch) => {
        console.log("\n[GITLAB API]: Create branch response:" + util.inspect(branch, false, null, true) + "\n");
      }).then(function() {
        API.RepositoryFiles.create(process.env.GITLAB_PROJECT, file, branch, content, commit_text).then((file) => {
          console.log("\n[GITLAB API]: Create proposal file response:" + util.inspect(file, false, null, true) + "\n");
        }).catch(error => {
          handlers.errorHandlerCreate(res, error);
        });
      }).then(function() {
        API.MergeRequests.create(process.env.GITLAB_PROJECT, branch, "master", merge_text).then((merge) => {
          console.log("\n[GITLAB API]: Create merge request response:" + util.inspect(merge, false, null, true) + "\n");

          mailer.getMailer(type, values, merge.web_url).sendMail({}, function(error, info) {
            if (error) {
              handlers.errorHandlerCreate(res, error);
            } else {
              res.redirect("/admin/");
            }
          });
        }).catch(error => {
          handlers.errorHandlerCreate(res, error);
        });
      }).catch(error => {
        handlers.errorHandlerCreate(res, error);
      });
  }
};
