"use strict";

module.exports = {
  app_name: "eslibre_app",
  event_name: "esLibre 2021: Petición de propuestas",
  max_size: 20971520,
  development: {
    dialect: "sqlite",
    storage: "./db.development.sqlite"
  },
  test: {
    dialect: "sqlite",
    storage: ":memory:"
  },
  production: {
    dialect: "mysql",
    host: "localhost",
    database: process.env.DB_DATABASE,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
  },
  auth: {
    enabled: true,
    user: process.env.LOGIN_USER,
    password: process.env.LOGIN_PASSWORD
  },
  email: {
    name: "esLibre",
    user: "tux@eslib.re",
    smtp: "ruido",
    server: "interferencias.tech",
    password: process.env.MAIL_PASSWORD,
    bcc: "Interferencias (Info) <info@interferencias.tech>",
    organization: "esLibre"
  },
  type: {
    talk: 0,
    workshop: 1,
    other: 2,
    devroom: 3
  }
};