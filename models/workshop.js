"use strict";

module.exports = (sequelize, DataTypes) => {
  var Workshop = sequelize.define("Workshop", {
    title: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(5000),
      allowNull: false
    },
    goals: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    project: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    target: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    speakers: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    web: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    mastodon: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    twitter: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    gitlab: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    github: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    prerequisites_attendees: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    prerequisites_organization: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    length: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    day: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    comments: {
      type: DataTypes.STRING(500),
      allowNull: true
    },
    privacy_email: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    privacy_social: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    accept_coc: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    accept_attend: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    accept_coordination: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    state: {
      // R = Received, A = Accepted, D = Denied
      type: DataTypes.CHAR(1),
      allowNull: false
    }
  });

  return Workshop;
};
