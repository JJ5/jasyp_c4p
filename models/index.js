"use strict";

var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");

var config = require(path.join(__dirname, "..", "config", "config"));

var basename = path.basename(__filename);
var db = {};
var env = process.env.NODE_ENV || "development";
var sequelize;

if (env.localeCompare("development") === 0) {
  sequelize = new Sequelize("debug", null, null, config.development);
} else {
  sequelize = new Sequelize(config.production.database, config.production.username, config.production.password, config.production);
}

fs.readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf(".") !== 0) && (file !== basename) && (file.slice(-3) === ".js");
  })
  .forEach(file => {
    var model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes)

    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
